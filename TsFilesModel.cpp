#include "TsFilesModel.h"

#include <QIcon>

TsFilesModel::TsFilesModel( QObject* parent )
    : QAbstractTableModel( parent )
{
}

TsFilesModel::~TsFilesModel()
{
}

void TsFilesModel::setProcessingStatus( const QString& tsFilePath, const CommonEntities::ProcessingStatus processingStatus )
{
    if ( tsFilePath.isEmpty()
         || ! CommonEntities::isProcessingStatusValid( processingStatus ) )
    {
        return;
    }

    const int existingTsFileRowIndex = m_tsFilePathToRowIndexMappings.value( tsFilePath, -1 );
    if ( ! this->isRowIndexValid( existingTsFileRowIndex ) )
    {
        return;
    }

    CommonEntities::ProcessingStatus& existingProcessingStatus = m_tsFilesPathProcessingStatus[ existingTsFileRowIndex ].second;
    if ( processingStatus == existingProcessingStatus )
    {
        return;
    }

    existingProcessingStatus = processingStatus;
    const QModelIndex modelIndex = this->index( existingTsFileRowIndex, ColumnIndexProcessingStatus );
    emit dataChanged( modelIndex, modelIndex );
}

void TsFilesModel::addOrReplace( const QList< QPair< QString, CommonEntities::ProcessingStatus > >& aTsFilesPathProcessingStatus )
{
    QList< QPair< QString, CommonEntities::ProcessingStatus > > tsFilesPathProcessingStatusToAdd;
    QHash< QString, int > tsFilePathToRowIndexMappingsToAdd;
    for ( const QPair< QString, CommonEntities::ProcessingStatus >& tsFilePathProcessingStatus : aTsFilesPathProcessingStatus )
    {
        if ( tsFilePathProcessingStatus.first.isEmpty()
             || ! CommonEntities::isProcessingStatusValid( tsFilePathProcessingStatus.second ) )
        {
            continue;
        }

        const int existingTsFileRowIndex = m_tsFilePathToRowIndexMappings.value( tsFilePathProcessingStatus.first, -1 );
        if ( this->isRowIndexValid( existingTsFileRowIndex ) )
        {
            CommonEntities::ProcessingStatus& existingProcessingStatus = m_tsFilesPathProcessingStatus[ existingTsFileRowIndex ].second;
            if ( tsFilePathProcessingStatus.second == existingProcessingStatus )
            {
                continue;
            }

            existingProcessingStatus = tsFilePathProcessingStatus.second;
            const QModelIndex modelIndex = this->index( existingTsFileRowIndex, ColumnIndexProcessingStatus );
            emit dataChanged( modelIndex, modelIndex );

            continue;
        }

        const int existingLocalRowIndex = tsFilePathToRowIndexMappingsToAdd.value( tsFilePathProcessingStatus.first, -1 );
        if ( existingLocalRowIndex >= 0 )
        {
            tsFilesPathProcessingStatusToAdd[ existingLocalRowIndex ].second = tsFilePathProcessingStatus.second;
        }
        else
        {
            tsFilesPathProcessingStatusToAdd.append( tsFilePathProcessingStatus );
            tsFilePathToRowIndexMappingsToAdd.insert( tsFilePathProcessingStatus.first, tsFilesPathProcessingStatusToAdd.size() - 1 );
        }
    }

    const int tsFilesPathProcessingStatusToAddCount = tsFilesPathProcessingStatusToAdd.size();
    if ( tsFilesPathProcessingStatusToAddCount == 0 )
    {
        return;
    }

    const int rowsCount = this->rowCount();
    this->beginInsertRows( QModelIndex(), rowsCount, rowsCount + tsFilesPathProcessingStatusToAddCount - 1 );

    for ( int i = 0; i < tsFilesPathProcessingStatusToAddCount; ++ i )
    {
        const QPair< QString, CommonEntities::ProcessingStatus >& tsFilePathProcessingStatus = aTsFilesPathProcessingStatus[ i ];
        m_tsFilesPathProcessingStatus.append( tsFilePathProcessingStatus );
        m_tsFilePathToRowIndexMappings[ tsFilePathProcessingStatus.first ] = i + rowsCount;
    }

    this->endInsertRows();
}

void TsFilesModel::set( const QList< QPair< QString, CommonEntities::ProcessingStatus > >& aTsFilesPathProcessingStatus )
{
    this->clear();
    this->addOrReplace( aTsFilesPathProcessingStatus );
}

void TsFilesModel::clear()
{
    if ( m_tsFilesPathProcessingStatus.isEmpty() )
    {
        return;
    }

    this->beginResetModel();
    m_tsFilesPathProcessingStatus.clear();
    this->endResetModel();
}

bool TsFilesModel::isRowIndexValid( const int rowIndex ) const
{
    return 0 <= rowIndex && rowIndex < this->rowCount();
}

bool TsFilesModel::isColumnIndexValid( const int columnIndex ) const
{
    return 0 <= columnIndex && columnIndex < ColumnsCount;
}

int TsFilesModel::rowCount( const QModelIndex& parent ) const
{
    Q_UNUSED( parent )

    return m_tsFilesPathProcessingStatus.size();
}

int TsFilesModel::columnCount( const QModelIndex& parent ) const
{
    Q_UNUSED( parent )

    return ColumnsCount;
}

QVariant TsFilesModel::headerData( int section, Qt::Orientation orientation, int role ) const
{
    if ( orientation == Qt::Horizontal
         && role == Qt::DisplayRole )
    {
        switch ( section )
        {
            case ColumnIndexFilePath:         return tr( "File" );
            case ColumnIndexProcessingStatus: return tr( "Status" );
            default:                          break;
        }
    }

    return QAbstractTableModel::headerData( section, orientation, role );
}

QVariant TsFilesModel::data( const QModelIndex& index, int role ) const
{
    const int rowIndex = index.row();
    const int columnIndex = index.column();
    if ( ! this->isRowIndexValid( rowIndex )
         || ! this->isColumnIndexValid( columnIndex ) )
    {
        return QVariant();
    }

    const QPair< QString, CommonEntities::ProcessingStatus >& tsFilePathProcessingStatus = m_tsFilesPathProcessingStatus[ rowIndex ];
    switch ( columnIndex )
    {
        case ColumnIndexFilePath:
            if ( role == Qt::DisplayRole
                 || role == Qt::UserRole )
            {
                return tsFilePathProcessingStatus.first;
            }

            break;

        case ColumnIndexProcessingStatus:
        {
            switch ( role )
            {
                case Qt::DisplayRole:    return TsFilesModel::processingStatusDescription( tsFilePathProcessingStatus.second );
                case Qt::DecorationRole: return QIcon( TsFilesModel::processingStatusIconFilePath( tsFilePathProcessingStatus.second ) );
                case Qt::UserRole:       return static_cast< int >( tsFilePathProcessingStatus.second );
                default:                 break;
            }

            break;
        }

        default:
            break;
    }

    return QVariant();
}

QString TsFilesModel::processingStatusDescription( const CommonEntities::ProcessingStatus processingStatus )
{
    switch ( processingStatus )
    {
        case CommonEntities::ProcessingStatus::NotStarted: return tr( "Not Started" );
        case CommonEntities::ProcessingStatus::InProgress: return tr( "In Progress ..." );
        case CommonEntities::ProcessingStatus::ReadyDone:  return tr( "Done" );
        case CommonEntities::ProcessingStatus::ReadyError: return tr( "Error" );
        default:                                           break;
    }

    return QString();
}

QString TsFilesModel::processingStatusIconFilePath( const CommonEntities::ProcessingStatus processingStatus )
{
    switch ( processingStatus )
    {
        case CommonEntities::ProcessingStatus::NotStarted: return ":/not_started.png";
        case CommonEntities::ProcessingStatus::InProgress: return ":/in_progress.png";
        case CommonEntities::ProcessingStatus::ReadyDone:  return ":/done.png";
        case CommonEntities::ProcessingStatus::ReadyError: return ":/error.png";
        default:                                           break;
    }

    return QString();
}
