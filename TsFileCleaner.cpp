#include "TsFileCleaner.h"

#include <QDomDocument>
#include <QFile>
#include <QFileInfo>
#include <QStringBuilder>
#include <QtConcurrent/QtConcurrentRun>
#include <QTextStream>
#include <QUuid>
#include <QDir>

const QList< TsFileCleaner::XmlElementType > TsFileCleaner::s_allXmlElementTypes = QList< TsFileCleaner::XmlElementType >()
                                                                                   << TsFileCleaner::XmlElementType::Ts
                                                                                   << TsFileCleaner::XmlElementType::Context
                                                                                   << TsFileCleaner::XmlElementType::Message
                                                                                   << TsFileCleaner::XmlElementType::Translation;

const QString TsFileCleaner::s_tsTagName              = "TS";
const QString TsFileCleaner::s_contextTagName         = "context";
const QString TsFileCleaner::s_messageTagName         = "message";
const QString TsFileCleaner::s_translationTagName     = "translation";
const QString TsFileCleaner::s_typeAttributeName      = "type";
const QString TsFileCleaner::s_vanishedAttributeValue = "vanished";

TsFileCleaner::TsFileCleaner( QObject* const parent )
    : QObject( parent )
{
}

TsFileCleaner::~TsFileCleaner()
{
}

void TsFileCleaner::cleanTsFileAsync( const QString& tsFilePath )
{
    this->cleanTsFilesAsync( QStringList() << tsFilePath );
}

void TsFileCleaner::cleanTsFilesAsync( const QStringList& tsFilePaths )
{
    const int alreadyBeingProcessedTsFilesCount = m_allTsFileWatcherFilePathMappings.size();
    if ( alreadyBeingProcessedTsFilesCount > 0 )
    {
        if ( alreadyBeingProcessedTsFilesCount == m_readyTsFileWatchers.size() )
        {
            m_allTsFileWatcherFilePathMappings.clear();
            qDeleteAll( m_readyTsFileWatchers );
            m_readyTsFileWatchers.clear();
            m_tsFilePathProcessingStatusMappings.clear();
        }
        else
        {
            return;
        }
    }

    for ( const QString& tsFilePath : tsFilePaths )
    {
        m_tsFilePathProcessingStatusMappings.insert( tsFilePath, CommonEntities::ProcessingStatus::NotStarted );
        QFutureWatcher< bool >* const tsFileWatcher = new QFutureWatcher< bool >( this );
        connect( tsFileWatcher, SIGNAL(started()), this, SLOT(processTsFileCleanupStarted()) );
        connect( tsFileWatcher, SIGNAL(finished()), this, SLOT(processTsFileCleanupFinished()) );
        m_allTsFileWatcherFilePathMappings.insert( tsFileWatcher, tsFilePath );

        tsFileWatcher->setFuture( QtConcurrent::run( & TsFileCleaner::cleanTsFile, tsFilePath ) );
    }
}

void TsFileCleaner::processTsFileCleanupFinished()
{
    QFutureWatcher< bool >* const tsFileWatcher = dynamic_cast< QFutureWatcher< bool >* >( this->sender() );
    if ( tsFileWatcher == nullptr )
    {
        return;
    }

    this->setTsFileProcessingStatus( tsFileWatcher, tsFileWatcher->result()
                                                    ? CommonEntities::ProcessingStatus::ReadyDone
                                                    : CommonEntities::ProcessingStatus::ReadyError );
}

void TsFileCleaner::processTsFileCleanupStarted()
{
    this->setTsFileProcessingStatus( dynamic_cast< QFutureWatcher< bool >* >( this->sender() ),
                                     CommonEntities::ProcessingStatus::InProgress );
}

bool TsFileCleaner::cleanTsFile( const QString& tsFilePath )
{
    QFile tsFile( tsFilePath );
    if ( ! tsFile.open( QIODevice::ReadOnly ) )
    {
        return false;
    }

    QDomDocument tsFileXmlDoc;
    QString errorMessage;
    int errorRow = -1;
    int errorColumn = -1;

    if ( ! tsFileXmlDoc.setContent( & tsFile, true, & errorMessage, & errorRow, & errorColumn ) )
    {
        qDebug() << "error: message(" << errorMessage << "), row(" << errorRow << "), column(" << errorColumn << ")";
        return false;
    }

    QDomElement tsFileMainXmlElement = tsFileXmlDoc.documentElement();
    if ( TsFileCleaner::cleanXmlElement( tsFileMainXmlElement ) )
    {
        const QString expectedChildXmlElementName = TsFileCleaner::innerXmlElementName( TsFileCleaner::xmlElementTypeFromName( tsFileMainXmlElement.tagName() ) );
        if ( tsFileMainXmlElement.firstChildElement( expectedChildXmlElementName ).isNull() )
        {
            tsFileXmlDoc.removeChild( tsFileMainXmlElement );
        }
    }

    tsFile.close();
    if ( ! tsFile.open( QIODevice::WriteOnly | QIODevice::Truncate ) )
    {
        return false;
    }

    QTextStream ts( & tsFile );
    tsFileXmlDoc.save( ts, 4 );
    tsFile.close();

    return true;
}

bool TsFileCleaner::cleanXmlElement( QDomElement& xmlElement )
{
    QList< QDomElement > childrenToRemove;
    QString expectedChildXmlElementName;
    const XmlElementType xmlElementType = TsFileCleaner::xmlElementTypeFromName( xmlElement.tagName() );
    switch ( xmlElementType )
    {
        case XmlElementType::Ts:      // don't break
        case XmlElementType::Context: // don't break
        case XmlElementType::Message:
        {
            expectedChildXmlElementName = TsFileCleaner::innerXmlElementName( xmlElementType );
            if ( expectedChildXmlElementName.isEmpty() )
            {
                return false;
            }

            for ( QDomElement childXmlElement = xmlElement.firstChildElement( expectedChildXmlElementName );
                  ! childXmlElement.isNull();
                  childXmlElement = childXmlElement.nextSiblingElement( expectedChildXmlElementName ) )
            {
                if ( ! TsFileCleaner::cleanXmlElement( childXmlElement ) )
                {
                    continue;
                }

                switch ( xmlElementType )
                {
                    case XmlElementType::Ts:      // don't break
                    case XmlElementType::Context:
                        childrenToRemove.append( childXmlElement );
                        break;

                    case XmlElementType::Message:
                        return true;
                }
            }

            break;
        }
        case XmlElementType::Translation:
            return xmlElement.attribute( s_typeAttributeName ) == s_vanishedAttributeValue;
    }

    for ( const QDomElement& childToRemove : childrenToRemove )
    {
        xmlElement.removeChild( childToRemove );
    }

    return xmlElement.firstChildElement( expectedChildXmlElementName ).isNull();
}

void TsFileCleaner::setTsFileProcessingStatus(
        const QString& tsFilePath,
        const CommonEntities::ProcessingStatus processingStatus )
{
    if ( ! CommonEntities::isProcessingStatusValid( processingStatus ) )
    {
        return;
    }

    const QHash< QString, CommonEntities::ProcessingStatus >::iterator tsFilePathProcessingStatusMappingsIter = m_tsFilePathProcessingStatusMappings.find( tsFilePath );
    if ( tsFilePathProcessingStatusMappingsIter == m_tsFilePathProcessingStatusMappings.end() )
    {
        return;
    }

    CommonEntities::ProcessingStatus& existingProcessingStatus = tsFilePathProcessingStatusMappingsIter.value();
    if ( processingStatus == existingProcessingStatus )
    {
        return;
    }

    existingProcessingStatus = processingStatus;

    emit tsFileProcessingStatusChanged( tsFilePath, processingStatus );

}

void TsFileCleaner::setTsFileProcessingStatus(
        QFutureWatcher< bool >* const tsFileWatcher,
        const CommonEntities::ProcessingStatus processingStatus )
{
    if ( tsFileWatcher == nullptr )
    {
        return;
    }

    const QHash< QFutureWatcher< bool >*, QString >::const_iterator allTsFileWatcherFilePathMappingsIter = m_allTsFileWatcherFilePathMappings.constFind( tsFileWatcher );
    if ( allTsFileWatcherFilePathMappingsIter == m_allTsFileWatcherFilePathMappings.constEnd() )
    {
        return;
    }

    this->setTsFileProcessingStatus( allTsFileWatcherFilePathMappingsIter.value(), processingStatus );

    if ( processingStatus == CommonEntities::ProcessingStatus::ReadyDone
         || processingStatus == CommonEntities::ProcessingStatus::ReadyError )
    {
        m_readyTsFileWatchers.insert( tsFileWatcher );
    }
}

TsFileCleaner::XmlElementType TsFileCleaner::xmlElementTypeFromName( const QString& xmlElementName )
{
    static QHash< QString, TsFileCleaner::XmlElementType > mappings;
    if ( mappings.isEmpty() )
    {
        mappings.reserve( s_allXmlElementTypes.size() );
        for ( const XmlElementType xmlElementType : s_allXmlElementTypes )
        {
            mappings[ TsFileCleaner::xmlElementNameFromType( xmlElementType ) ] = xmlElementType;
        }
    }

    return mappings.value( xmlElementName, XmlElementType::Unknown );
}

QString TsFileCleaner::xmlElementNameFromType( const TsFileCleaner::XmlElementType xmlElementType )
{
    switch ( xmlElementType )
    {
        case XmlElementType::Ts:          return s_tsTagName;
        case XmlElementType::Context:     return s_contextTagName;
        case XmlElementType::Message:     return s_messageTagName;
        case XmlElementType::Translation: return s_translationTagName;
    }

    return QString();
}

TsFileCleaner::XmlElementType TsFileCleaner::innerXmlElementType( const TsFileCleaner::XmlElementType outerXmlElementType )
{
    switch ( outerXmlElementType )
    {
        case XmlElementType::Ts:          return XmlElementType::Context;
        case XmlElementType::Context:     return XmlElementType::Message;
        case XmlElementType::Message:     return XmlElementType::Translation;
        case XmlElementType::Translation: return XmlElementType::Unknown;
    }

    return XmlElementType::Unknown;
}

QString TsFileCleaner::innerXmlElementName( const TsFileCleaner::XmlElementType outerXmlElementType )
{
    return TsFileCleaner::xmlElementNameFromType( TsFileCleaner::innerXmlElementType( outerXmlElementType ) );
}
