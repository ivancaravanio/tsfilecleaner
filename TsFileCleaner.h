#ifndef TSFILECLEANER_H
#define TSFILECLEANER_H

#include "CommonEntities.h"

#include <QFutureWatcher>
#include <QHash>
#include <QList>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QtXml/QDomElement>

class TsFileCleaner : public QObject
{
    Q_OBJECT

public:
    explicit TsFileCleaner( QObject* parent = nullptr );
    ~TsFileCleaner() Q_DECL_OVERRIDE;

    void cleanTsFileAsync( const QString& tsFilePath );
    void cleanTsFilesAsync( const QStringList& tsFilePaths );

    inline const QHash< QString, CommonEntities::ProcessingStatus >& tsFilePathProcessingStatusMappings() const
    {
        return m_tsFilePathProcessingStatusMappings;
    }

signals:
    void tsFileProcessingStatusChanged( const QString& tsFilePath, const CommonEntities::ProcessingStatus processingStatus );

private:
    enum class XmlElementType
    {
        Unknown,
        Ts,
        Context,
        Message,
        Translation
    };

private:
    static const QList< XmlElementType > s_allXmlElementTypes;

    static const QString s_tsTagName;
    static const QString s_contextTagName;
    static const QString s_messageTagName;
    static const QString s_translationTagName;
    static const QString s_typeAttributeName;
    static const QString s_vanishedAttributeValue;

private slots:
    void processTsFileCleanupStarted();
    void processTsFileCleanupFinished();

private:
    static bool cleanTsFile( const QString& tsFilePath );
    static bool cleanXmlElement( QDomElement& xmlElement );
    void setTsFileProcessingStatus( const QString& tsFilePath, const CommonEntities::ProcessingStatus processingStatus );
    void setTsFileProcessingStatus( QFutureWatcher< bool >* const tsFileWatcher, const CommonEntities::ProcessingStatus processingStatus );
    static XmlElementType xmlElementTypeFromName( const QString& xmlElementName );
    static QString xmlElementNameFromType( const XmlElementType xmlElementType );

    static XmlElementType innerXmlElementType( const XmlElementType outerXmlElementType );
    static QString innerXmlElementName( const XmlElementType outerXmlElementType );

private:
    QHash< QString, CommonEntities::ProcessingStatus > m_tsFilePathProcessingStatusMappings;
    QHash< QFutureWatcher< bool >*, QString >          m_allTsFileWatcherFilePathMappings;
    QSet< QFutureWatcher< bool >* >                    m_readyTsFileWatchers;
};

#endif // TSFILECLEANER_H
