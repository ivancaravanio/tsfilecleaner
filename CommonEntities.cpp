#include "CommonEntities.h"

bool CommonEntities::isProcessingStatusValid( const CommonEntities::ProcessingStatus processingStatus )
{
    return processingStatus == ProcessingStatus::NotStarted
           || processingStatus == ProcessingStatus::InProgress
           || processingStatus == ProcessingStatus::ReadyDone
           || processingStatus == ProcessingStatus::ReadyError;
}
