#ifndef WIDGET_H
#define WIDGET_H

#include <QString>
#include <QStringList>
#include <QWidget>

class TsFileCleaner;
class TsFilesModel;

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void selectTsFiles();

private:
    void cleanTsFiles( const QStringList& tsFilePaths );

private:
    TsFileCleaner* m_tsFileCleaner;
    TsFilesModel*  m_tsFilesModel;
};

#endif // WIDGET_H
