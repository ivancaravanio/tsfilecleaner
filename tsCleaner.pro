QT += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = tsCleaner
TEMPLATE = app

CONFIG += c++11

SOURCES += \
    CommonEntities.cpp \
    main.cpp \
    TsFileCleaner.cpp \
    TsFilesModel.cpp \
    Widget.cpp

HEADERS += \
    CommonEntities.h \
    TsFileCleaner.h \
    TsFilesModel.h \
    Widget.h

RESOURCES += \
    Resources/Resources.qrc

