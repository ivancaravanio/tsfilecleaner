#include "Widget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName( QObject::tr( "TS File Cleaner" ) );
    Widget w;
    w.show();

    return a.exec();
}
