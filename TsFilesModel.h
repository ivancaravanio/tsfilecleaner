#ifndef TSFILESMODEL_H
#define TSFILESMODEL_H

#include "CommonEntities.h"

#include <QAbstractTableModel>
#include <QList>
#include <QString>
#include <QPair>
#include <QHash>

class TsFilesModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum ColumnIndex
    {
        ColumnIndexInvalid          = -1,
        ColumnIndexFilePath         = 0,
        ColumnIndexProcessingStatus = 1,
        ColumnsCount                = 2
    };

public:
    TsFilesModel( QObject* parent = nullptr );
    ~TsFilesModel() Q_DECL_OVERRIDE;

    void set( const QList< QPair< QString, CommonEntities::ProcessingStatus > >& aTsFilesPathProcessingStatus );
    void addOrReplace( const QList< QPair< QString, CommonEntities::ProcessingStatus > >& aTsFilesPathProcessingStatus );
    void clear();

    bool isRowIndexValid( const int rowIndex ) const;
    bool isColumnIndexValid( const int columnIndex ) const;

    int rowCount( const QModelIndex& parent = QModelIndex() ) const Q_DECL_OVERRIDE;
    int columnCount( const QModelIndex& parent = QModelIndex() ) const Q_DECL_OVERRIDE;

    QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const Q_DECL_OVERRIDE;
    QVariant data( const QModelIndex& index, int role = Qt::DisplayRole ) const Q_DECL_OVERRIDE;

public slots:
    void setProcessingStatus( const QString& tsFilePath, const CommonEntities::ProcessingStatus processingStatus );

private:
    static QString processingStatusDescription( const CommonEntities::ProcessingStatus processingStatus );
    static QString processingStatusIconFilePath( const CommonEntities::ProcessingStatus processingStatus );

private:
    QList< QPair< QString, CommonEntities::ProcessingStatus > > m_tsFilesPathProcessingStatus;
    QHash< QString, int > m_tsFilePathToRowIndexMappings;
};

#endif // TSFILESMODEL_H
