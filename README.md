A multithreaded Qt XML translation files (*.ts) cleaner. Removes any translations that are marked as "obsolete" or "vanished".
Qt [translation documentation](http://doc.qt.io/qt-5/linguist-translators.html) comments on "obsolete" translations as follows ("vanished" ones are surprisingly not mentioned in Qt 5.5 documentation, since [they have been introduced from Qt 5.2 on](http://doc.qt.io/qt-5/linguist-ts-file-format.html) ):

"The string is obsolete. It is no longer used in the context. See the Release Manager for instructions on how to remove obsolete messages from the file."

So, the [Release Manager documentation](http://doc.qt.io/qt-5/linguist-manager.html) doesn't mention anything about how to remove those translations. But when one executes lupdate there is a command-line option "-no-obsolete" that removes them. So, I wrote this tool after I saw this option. Anyway, it was a good exercise.