#ifndef COMMONENTITIES_H
#define COMMONENTITIES_H

#include <qcompilerdetection.h>

class CommonEntities
{
public:
    enum class ProcessingStatus
    {
        NotStarted,
        InProgress,
        ReadyDone,
        ReadyError
    };

public:
    CommonEntities() Q_DECL_EQ_DELETE;

    static bool isProcessingStatusValid( const ProcessingStatus processingStatus );

};

#endif // COMMONENTITIES_H
