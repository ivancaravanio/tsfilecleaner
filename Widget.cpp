#include "Widget.h"

#include "TsFileCleaner.h"
#include "TsFilesModel.h"

#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QPushButton>
#include <QTableView>
#include <QVBoxLayout>

Widget::Widget( QWidget* parent )
    : QWidget( parent )
    , m_tsFileCleaner( nullptr )
    , m_tsFilesModel( nullptr )
{
    QPushButton* const selectFilesButton = new QPushButton( "Select Files ..." );
    connect( selectFilesButton, SIGNAL(clicked()), this, SLOT(selectTsFiles()) );

    m_tsFileCleaner = new TsFileCleaner( this );
    m_tsFilesModel = new TsFilesModel( this );
    connect( m_tsFileCleaner, SIGNAL(tsFileProcessingStatusChanged(QString,CommonEntities::ProcessingStatus)),
             m_tsFilesModel, SLOT(setProcessingStatus(QString,CommonEntities::ProcessingStatus)) );

    QTableView* const tsFilesView = new QTableView;
    tsFilesView->setModel( m_tsFilesModel );

    QVBoxLayout* const mainLayout = new QVBoxLayout( this );
    mainLayout->addWidget( selectFilesButton, 0, Qt::AlignCenter );
    mainLayout->addWidget( tsFilesView );
}

Widget::~Widget()
{
}

void Widget::selectTsFiles()
{
    const QStringList tsFilePaths = QFileDialog::getOpenFileNames( this, "Select .ts Files ...", QString(), "*.ts" );
    this->cleanTsFiles( tsFilePaths );
}

void Widget::cleanTsFiles( const QStringList& tsFilePaths )
{
    QList< QPair< QString, CommonEntities::ProcessingStatus > > tsFilePathProcessingStatusPairs;
    QStringList canonicalTsFilePaths;

    for ( const QString& tsFilePath : tsFilePaths )
    {
        const QFileInfo tsFileInfo( tsFilePath );
        if ( ! tsFileInfo.exists() )
        {
            return;
        }

        const QString canonicalTsFilePath = QDir::toNativeSeparators( tsFileInfo.canonicalFilePath() );
        tsFilePathProcessingStatusPairs.append( QPair< QString, CommonEntities::ProcessingStatus >( canonicalTsFilePath, CommonEntities::ProcessingStatus::NotStarted ) );
        canonicalTsFilePaths.append( canonicalTsFilePath );
    }

    m_tsFilesModel->set( tsFilePathProcessingStatusPairs );
    m_tsFileCleaner->cleanTsFilesAsync( canonicalTsFilePaths );
}
